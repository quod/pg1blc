/**
 * Consumer.java
 *
 * This is the consumer thread for the bounded buffer problem.
 *
 * @author Ben Carson
 * CS350 Programming Assignment 1
 * September 21, 2016
 */

import java.util.*;

public class Consumer extends Thread {
    private BoundedBuffer buffer;
    private String name;

    public Consumer(String s, BoundedBuffer b) {
        name = s;
        buffer = b;
    }
    
    // checks whether a given integer is prime
    public boolean isPrime(int p) {
        for (int i = 2; i < p; i++) {
            if (p % i == 0) {
                return false;
            }
        }
        return true;
    }

    public void run() {
        int random_integer;

        while (true) {
            int sleeptime = (int) (BoundedBuffer.NAP_TIME * Math.random()) +1;

            System.out.println("Consumer " + name + " sleeping for " + sleeptime + " seconds");

            try {
                sleep(sleeptime*1000);
            }
            catch(InterruptedException e) {}

            // consume an item from the buffer
            System.out.println("Consumer " + name + " wants to consume.");

            Object object = buffer.remove();
            
            if (object != null) {
                random_integer = (int) object;
                System.out.println("Consumer " + name + " consumed " + random_integer + 
                " and it is" + (isPrime(random_integer) ? " " : " not ") +
                "a prime number.");
            }
        }
    }
}
