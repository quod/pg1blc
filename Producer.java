/**
 * Producer.java
 *
 * This is the producer thread for the bounded buffer problem.
 *
 * @author Ben Carson
 * CS350 Programming Assignment 1
 * September 21, 2016
 */

import java.util.*;

public class Producer extends Thread {
    private BoundedBuffer buffer;
    private String name;

    public Producer(String s, BoundedBuffer b) {
        name = s;
        buffer = b;
    }

    public void run() {
        int random_integer;

        while (true) {
            int sleeptime = (int) (BoundedBuffer.NAP_TIME * Math.random()) +1;
            System.out.println("Producer " + name + " sleeping for " + sleeptime + " seconds");

            try {
                sleep(sleeptime*1000);
            }

            catch(InterruptedException e) {}

        // generate a random number and put it into the buffer
        Random rand = new Random();
        random_integer = (int) (4000 + (56000 * Math.random()));
        System.out.println("Producer " + name + " produced " + random_integer);
        buffer.enter(random_integer);
        }
    }
}
