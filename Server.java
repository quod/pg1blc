/**
 * Server.java
 *
 * This creates the buffer and the producer and consumer threads.
 *
 * @author Ben Carson
 * CS350 Programming Assignment 1
 * September 21, 2016
 */

public class Server
{
	public static void main(String args[]) {
		// create the buffers to be shared
		BoundedBuffer server0 = new BoundedBuffer();
		BoundedBuffer server1 = new BoundedBuffer();

   		// create the producer and consumer threads
   		Producer producerThread0 = new Producer("John", server0);
		Producer producerThread1 = new Producer("Liz" , server1);
   		Consumer consumerThread0 = new Consumer("Mary", server0);
		Consumer consumerThread1 = new Consumer("Bert", server1);

		// start the producer and consumer threads
   		producerThread0.start();
   		producerThread1.start();
   		consumerThread0.start();
   		consumerThread1.start();

	}//main
}//class
